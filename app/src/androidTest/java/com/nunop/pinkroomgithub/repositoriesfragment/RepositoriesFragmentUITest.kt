package com.nunop.pinkroomgithub.repositoriesfragment

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nunop.pinkroomgithub.MainActivity
import com.nunop.pinkroomgithub.R
import com.nunop.pinkroomgithub.api.ApiClient
import com.nunop.pinkroomgithub.utilities.EspressoIdlingResource
import com.nunop.pinkroomgithub.utils.AndroidTestUtils.withRecyclerView
import com.nunop.pinkroomgithub.utils.MockServerDispatcherAndroid
import com.nunop.pinkroomgithub.utils.RecyclerViewItemCountAssertion
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RepositoriesFragmentUITest {
    @Rule
    @JvmField
    var activityTestRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    companion object {

        fun hasItemCount(count: Int): ViewAssertion {
            return RecyclerViewItemCountAssertion(
                count
            )
        }

        lateinit var server: MockWebServer

        @BeforeClass
        @JvmStatic
        fun setup() {
            server = MockWebServer()
            server.start(9090)
            ApiClient.setBaseUrlPreview(server.url("/").toString())
        }

        @AfterClass
        @JvmStatic
        fun teardown() {
            server.shutdown()
        }
    }

    @Before
    fun init() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource())
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource())
    }

    @Test
    fun checkView() {
        server.setDispatcher(MockServerDispatcherAndroid().RequestDispatcher())

        Espresso.onView(ViewMatchers.withId(R.id.repositoriesList)).check(
            hasItemCount(
                40
            )
        )

        validateListContent(
            position = 0,
            repoName = "flutter",
            repoDescription = "Flutter makes it easy and fast to build beautiful apps for mobile and beyond.",
            language = "Dart",
            stars = "128333"
        )

        validateListContent(
            position = 1,
            repoName = "free-programming-books-zh_CN",
            repoDescription = ":books: 免费的计算机编程类中文书籍，欢迎投稿",
            language = null,
            stars = "82369"
        )
    }

    private fun validateListContent(
        position: Int,
        repoName: String?,
        repoDescription: String?,
        language: String?,
        stars: String?
    ) {
        repoName?.let {
            Espresso.onView(
                withRecyclerView(R.id.repositoriesList).atPositionOnView(
                    position,
                    R.id.tvRepoName
                )
            ).check(
                ViewAssertions.matches(ViewMatchers.withText(repoName))
            )
        }
        repoDescription?.let{
            Espresso.onView(
                withRecyclerView(R.id.repositoriesList).atPositionOnView(
                    position,
                    R.id.tvRepoDescription
                )
            ).check(
                ViewAssertions.matches(ViewMatchers.withText(repoDescription))
            )
        }

        language?.let{
            Espresso.onView(
                withRecyclerView(R.id.repositoriesList).atPositionOnView(
                    position,
                    R.id.tvLanguage
                )
            ).check(
                ViewAssertions.matches(ViewMatchers.withText(language))
            )
        }

        stars?.let{
            Espresso.onView(
                withRecyclerView(R.id.repositoriesList).atPositionOnView(
                    position,
                    R.id.tvNumberStars
                )
            ).check(
                ViewAssertions.matches(ViewMatchers.withText(stars))
            )
        }
    }
}