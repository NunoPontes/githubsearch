package com.nunop.pinkroomgithub.utils

object AndroidTestUtils {

    fun withRecyclerView(recyclerViewId: Int): RecyclerViewMatcher {
        return RecyclerViewMatcher(recyclerViewId)
    }
}