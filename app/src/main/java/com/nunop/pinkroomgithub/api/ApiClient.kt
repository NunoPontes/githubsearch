package com.nunop.pinkroomgithub.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.nunop.pinkroomgithub.BuildConfig
import com.nunop.pinkroomgithub.utilities.DateDeserializer
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

class ApiClient {
    companion object {
        private var BASE_URL_PREVIEW = "https://api.github.com"

        /**
         * This method returns retrofit client instance
         *
         * @return Retrofit object
         */
        fun getClient(): Retrofit {
            val trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as KeyStore?)
            val trustManagers = trustManagerFactory.trustManagers
            check(!(trustManagers.size != 1 || trustManagers[0] !is X509TrustManager)) {
                "Unexpected default trust managers:" + Arrays.toString(
                    trustManagers
                )
            }
            val trustManager = trustManagers[0] as X509TrustManager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, arrayOf<TrustManager>(trustManager), null)
            val clientBuilder = OkHttpClient.Builder()
            setClientBuilder(clientBuilder)
            clientBuilder.connectTimeout(120, TimeUnit.SECONDS)
            clientBuilder.readTimeout(120, TimeUnit.SECONDS)
            clientBuilder.sslSocketFactory(TLSSocketFactory(), trustManager)

            val gson = GsonBuilder()
                .registerTypeAdapter(
                    Date::class.java,
                    DateDeserializer("yyyy-MM-dd'T'HH:mm:ss")
                )
                .create()
            return Retrofit.Builder()
                .baseUrl(BASE_URL_PREVIEW)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(clientBuilder.build())
                .build()
        }

        private fun setClientBuilder(clientBuilder: OkHttpClient.Builder) {
            if (BuildConfig.DEBUG) {
                clientBuilder.addNetworkInterceptor(StethoInterceptor())
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                clientBuilder.addInterceptor(httpLoggingInterceptor)
            }
            val interceptor = { chain: Interceptor.Chain ->
                val request = chain.request()
                //Response
                val response = chain.proceed(request)
                val responseBody = response.body()
                val source = responseBody!!.source()
                source.request(java.lang.Long.MAX_VALUE)

                response
            }

            clientBuilder.addInterceptor(interceptor)
        }

        fun setBaseUrlPreview(baseUrlPreview: String) {
            BASE_URL_PREVIEW = baseUrlPreview
        }

    }
}