package com.nunop.pinkroomgithub.api

import com.nunop.pinkroomgithub.models.repository.Repository
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {


    @GET("/search/repositories")
    fun searchRepositories(@Query("q") query: String,
                           @Query("sort") sort: String,
                           @Query("order") order: String,
                           @Query("page") numPage: Int,
                           @Query("per_page") pageSize: Int):
            Observable<Response<Repository>>


}