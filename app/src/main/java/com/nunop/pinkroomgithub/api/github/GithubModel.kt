package com.nunop.pinkroomgithub.api.github

import com.nunop.pinkroomgithub.api.ApiClient
import com.nunop.pinkroomgithub.api.ApiInterface
import com.nunop.pinkroomgithub.models.repository.Repository
import io.reactivex.Observable
import retrofit2.Response

class GithubModel {


    /**
     * Gets list of repositories give query
     *
     * @param query string to be used as a query
     * @param sort string to sort results
     * @param order string asc or desc to determine the order
     * @param numPage page number
     * @param pageSize size of the page
     */
    fun searchRepositories(
        query: String,
        sort: String,
        order: String,
        numPage: Int,
        pageSize: Int
    ): Observable<Response<Repository>> {
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        return apiService.searchRepositories(query, sort, order, numPage, pageSize)
    }
}