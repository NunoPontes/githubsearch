package com.nunop.pinkroomgithub.models.repository

data class License(
    var html_url: String? = null,
    var key: String? = null,
    var name: String? = null,
    var node_id: String? = null,
    var spdx_id: String? = null,
    var url: String? = null
)