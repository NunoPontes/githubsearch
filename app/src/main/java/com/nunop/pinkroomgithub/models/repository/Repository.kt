package com.nunop.pinkroomgithub.models.repository

data class Repository(
    var incomplete_results: Boolean? = null,
    var items: List<Item>? = null,
    var total_count: Int? = null
)