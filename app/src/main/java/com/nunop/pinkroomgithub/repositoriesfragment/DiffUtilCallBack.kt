package com.nunop.pinkroomgithub.repositoriesfragment

import androidx.recyclerview.widget.DiffUtil
import com.nunop.pinkroomgithub.models.repository.Item

class DiffUtilCallBack : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem == newItem
    }

}