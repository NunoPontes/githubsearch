package com.nunop.pinkroomgithub.repositoriesfragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.nunop.pinkroomgithub.api.github.GithubModel
import com.nunop.pinkroomgithub.databinding.FragmentRepositoriesBinding
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.repositoriesfragment.adapters.RepositoriesAdapter
import com.nunop.pinkroomgithub.repositoriesfragment.viewmodels.RepositoriesViewModel
import com.nunop.pinkroomgithub.repositoriesfragment.viewmodels.RepositoriesViewModelFactory
import com.nunop.pinkroomgithub.utilities.EspressoIdlingResource
import com.nunop.pinkroomgithub.utilities.LiveDataState
import com.nunop.pinkroomgithub.utilities.toVisibilityGone

class RepositoriesFragment : Fragment(),
    RepositoriesFragmentView, RepositoriesAdapter.OnRepositoryListener {

    private lateinit var mRepositoriesAdapter: RepositoriesAdapter
    private lateinit var mViewModel: RepositoriesViewModel
    private lateinit var mPresenter: RepositoriesFragmentPresenter
    private var _binding: FragmentRepositoriesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRepositoriesBinding.inflate(inflater, container, false)

        EspressoIdlingResource.increment()

        mRepositoriesAdapter = RepositoriesAdapter(
            this
        )
        mPresenter = RepositoriesFragmentPresenterImpl(
            this,
            GithubModel()
        )
        mViewModel = ViewModelProvider(
            this,
            RepositoriesViewModelFactory(mPresenter)
        ).get(RepositoriesViewModel::class.java)

        binding.repositoriesSwipeRefreshLayout.isRefreshing = false
        binding.repositoriesList.adapter = mRepositoriesAdapter
        observeLiveData()

        binding.repositoriesSwipeRefreshLayout.setOnRefreshListener {
            binding.repositoriesSwipeRefreshLayout.isRefreshing = true
            mPresenter.resetRepositoryList()
        }

        mPresenter.refreshData()

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        binding.repositoriesSwipeRefreshLayout.isEnabled = true
    }

    override fun onPause() {
        super.onPause()
        binding.repositoriesSwipeRefreshLayout.isEnabled = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.repositoriesSwipeRefreshLayout.isEnabled = false
        removeLiveDataObservers()
        mPresenter.onViewDetached()
        binding.repositoriesList.adapter = null
        _binding = null
    }

    override fun setMenuItem(menu: Menu, index: Int, drawable: Int) {
        menu.getItem(index).icon =
            AppCompatResources.getDrawable(requireContext(), drawable)
    }

    override fun listIsInvalidated(): Boolean {
        return mViewModel.listIsInvalid()
    }

    override fun listIsEmpty(): Boolean {
        return mViewModel.listIsEmpty()
    }

    override fun setProgressBarVisibility(show: Boolean) {
        binding.pbRepositories.visibility = show.toVisibilityGone()
    }

    override fun addInitResultsAndRequestMore(
        callback: PageKeyedDataSource.LoadInitialCallback<Int, Item>?,
        response: List<Item>,
        previousPageKey: Int?,
        nextPageKey: Int
    ) {
        callback?.onResult(
            response, previousPageKey, nextPageKey
        )
    }

    override fun addAfterResultsAndRequestMore(
        callback: PageKeyedDataSource.LoadCallback<Int, Item>?,
        response: List<Item>,
        adjacentPageKey: Int
    ) {
        callback?.onResult(
            response,
            adjacentPageKey
        )
    }

    override fun setAdapterState(state: LiveDataState) {
        mRepositoriesAdapter.setState(state)
    }

    override fun setListVisibility(show: Boolean) {
        binding.repositoriesList.visibility = show.toVisibilityGone()
    }

    override fun onRepositoryClick(position: Int) {
        //val repository = mRepositoriesAdapter.currentList?.get(position)
        //TODO:click if i have the time
    }

    override fun setItemVisibility(item: MenuItem?, visibility: Boolean) {
        item?.isVisible = visibility
    }

    override fun observeLiveData() {
        //observe live data emitted by view model
        mViewModel.repositoriesLiveData?.observe(viewLifecycleOwner, {
            binding.repositoriesSwipeRefreshLayout.isRefreshing = false
            submitList(it)
        })

        mViewModel.getState().observe(viewLifecycleOwner, { state ->
            mPresenter.stateObserver(state as LiveDataState)
        })
    }

    override fun submitList(listRepositories: PagedList<Item>?) {
        mRepositoriesAdapter.submitList(listRepositories)
        EspressoIdlingResource.decrement()
    }

    override fun removeLiveDataObservers() {
        mViewModel.repositoriesLiveData?.removeObservers(viewLifecycleOwner)
        mViewModel.getState().removeObservers(viewLifecycleOwner)
    }

    override fun invalidateDataSource() {
        mViewModel.repositoriesLiveData?.value?.dataSource?.invalidate()
    }

}
