package com.nunop.pinkroomgithub.repositoriesfragment

import androidx.paging.PageKeyedDataSource
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.models.repository.Repository
import com.nunop.pinkroomgithub.utilities.LiveDataState
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import retrofit2.Response

interface RepositoriesFragmentPresenter {

    /**
     * Get the observer behaviour for the states of the list
     *
     * @param state state of the list
     */
    fun stateObserver(state: LiveDataState?)

    /**
     * Add the requests to a disposable
     *
     * @param disposableContent disposable to be cleared
     */
    fun addDisposable(disposableContent: Disposable)

    /**
     * First API request made to fill the list
     *
     * @param pageSize list page size
     * @return observable to be subscribed
     */
    fun loadInitialRequest(pageSize: Int): Observable<Response<Repository>>

    /**
     * Following API requests made to fill the list
     *
     * @param pageNumber list page number
     * @param pageSize list page size
     * @return observable to be subscribed
     */
    fun loadAfterRequest(pageNumber: Int, pageSize: Int): Observable<Response<Repository>>

    /**
     * Subscribe behavior of the following requests
     *
     * @param body response body
     * @param adjacentPageKey next page number
     * @param pageSize page size
     * @param callback PageKeyed object
     */
    fun loadAfterRequestSubscribe(
        body: List<Item>?,
        adjacentPageKey: Int,
        pageSize: Int,
        callback: PageKeyedDataSource.LoadCallback<Int, Item>?
    )

    /**
     * Behavior when receiving code 200 in the load initial request
     *
     * @param body body of the response
     * @param pageSize page size
     * @param callback PageKeyed object
     */
    fun loadInitialCode200(
        body: List<Item>,
        pageSize: Int,
        callback: PageKeyedDataSource.LoadInitialCallback<Int, Item>?
    )

    /**
     * Clean disposables
     */
    fun onViewDetached()

    /**
     * Logs the error for the Reactive Network layer
     *
     * @param message error message
     */
    fun logReactiveNetworkError(message: String?)

    /**
     * Check if the after request should be performed
     *
     * @return true / false accordingly
     */
    fun afterRequestShouldBePerformed(): Boolean

    /**
     * Reset the Repository list
     *
     */
    fun resetRepositoryList()

    /**
     * Refreshes data
     */
    fun refreshData()
}