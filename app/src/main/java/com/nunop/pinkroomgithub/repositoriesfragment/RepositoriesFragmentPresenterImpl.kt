package com.nunop.pinkroomgithub.repositoriesfragment

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.nunop.pinkroomgithub.api.github.GithubModel
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.models.repository.Repository
import com.nunop.pinkroomgithub.utilities.LiveDataState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.Response
import java.util.concurrent.TimeUnit

class RepositoriesFragmentPresenterImpl(
    private val mView: RepositoriesFragmentView,
    private val mGithubModel: GithubModel
) : RepositoriesFragmentPresenter {

    private val disposable = CompositeDisposable()
    private var afterRequestAllowed = true

    override fun resetRepositoryList() {
        afterRequestAllowed = true
        mView.removeLiveDataObservers()
        mView.invalidateDataSource()
        mView.observeLiveData()
    }

    override fun refreshData() {
        addDisposable(Observable.interval(100000L, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                resetRepositoryList()
            })
    }

    override fun addDisposable(disposableContent: Disposable) {
        disposable.add(disposableContent)
    }

    override fun loadInitialRequest(pageSize: Int): Observable<Response<Repository>> {
        return makeRequest(1, pageSize)
    }

    override fun loadAfterRequest(
        pageNumber: Int,
        pageSize: Int
    ): Observable<Response<Repository>> {
        return makeRequest(pageNumber, pageSize)
    }

    override fun loadAfterRequestSubscribe(
        body: List<Item>?,
        adjacentPageKey: Int,
        pageSize: Int,
        callback: PageKeyedDataSource.LoadCallback<Int, Item>?
    ) {
        body?.let {
            afterRequestCheckAndUpdate(it, pageSize)
            mView.addAfterResultsAndRequestMore(callback, it, adjacentPageKey)
        }
    }

    override fun loadInitialCode200(
        body: List<Item>,
        pageSize: Int,
        callback: PageKeyedDataSource.LoadInitialCallback<Int, Item>?
    ) {
        afterRequestCheckAndUpdate(body, pageSize)
        mView.addInitResultsAndRequestMore(callback, body, null, 2)
    }

    override fun onViewDetached() {
        disposable.clear()
    }

    override fun logReactiveNetworkError(message: String?) {
        Log.e(this.javaClass.simpleName, message.toString())
    }

    override fun afterRequestShouldBePerformed(): Boolean {
        return afterRequestAllowed
    }

    override fun stateObserver(state: LiveDataState?) {
        val listInvalidated = mView.listIsInvalidated()
        val listEmpty = mView.listIsEmpty()

        when (state) {
            LiveDataState.ERROR -> errorStateBehaviour()
            LiveDataState.LOADING -> loadingStateBehaviour(listInvalidated, listEmpty)
            LiveDataState.NODATA -> noDataStateBehaviour()
            else -> otherStateBehaviour()
        }

        if (!mView.listIsEmpty()) {
            mView.setAdapterState(state ?: LiveDataState.DONE)
        }
    }

    private fun loadingStateBehaviour(listInvalidated: Boolean, listEmpty: Boolean) {
        if (listInvalidated) {
            mView.setListVisibility(false)
        } else {
            mView.setListVisibility(true)
        }

        if (listEmpty || listInvalidated) {
            mView.setProgressBarVisibility(true)
        } else {
            mView.setProgressBarVisibility(false)
        }
    }

    private fun noDataStateBehaviour() {
        mView.setListVisibility(true)
        mView.setProgressBarVisibility(false)

    }

    private fun errorStateBehaviour() {
        mView.setListVisibility(true)
        mView.setProgressBarVisibility(false)
    }


    private fun otherStateBehaviour() {
        mView.setListVisibility(true)
        mView.setProgressBarVisibility(false)
    }

    private fun makeRequest(
        pageNumber: Int,
        pageSize: Int
    ): Observable<Response<Repository>> {
        return mGithubModel.searchRepositories(
            query = "Android",
            sort = "stars",
            order = "desc",
            pageNumber,
            pageSize
        )
    }

    /**
     * Update the variable that checks if the next request is needed
     *
     * @param body body of the response
     * @param pageSize page size
     */
    private fun afterRequestCheckAndUpdate(body: List<Item>, pageSize: Int) {
        if (body.size < pageSize) {
            afterRequestAllowed = false
        }
    }
}
