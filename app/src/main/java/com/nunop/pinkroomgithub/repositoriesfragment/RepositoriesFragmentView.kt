package com.nunop.pinkroomgithub.repositoriesfragment

import android.view.Menu
import android.view.MenuItem
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.utilities.LiveDataState

interface RepositoriesFragmentView {

    /**
     * Check if list is invalidated
     *
     * @return true / false accordingly
     */
    fun listIsInvalidated(): Boolean

    /**
     * Check if list is empty
     *
     * @return true / false accordingly
     */
    fun listIsEmpty(): Boolean

    /**
     * Set progress bar visibility
     *
     * @param show true / false to show / hide
     */
    fun setProgressBarVisibility(show: Boolean)

    /**
     * Set state of the adapter
     *
     * @param state state implemented in the list
     */
    fun setAdapterState(state: LiveDataState)

    /**
     * Set list visibility
     *
     * @param show true / false to show / hide
     */
    fun setListVisibility(show: Boolean)

    /**
     * Add the results to the adapter (first request)
     *
     * @param callback PageKeyed object
     * @param response response from api
     * @param previousPageKey Key for page before the initial load result
     * @param nextPageKey Key for page after the initial load result
     */
    fun addInitResultsAndRequestMore(
        callback: PageKeyedDataSource.LoadInitialCallback<Int, Item>?,
        response: List<Item>,
        previousPageKey: Int?,
        nextPageKey: Int
    )

    /**
     * Add the results to the adapter (following requests)
     *
     * @param callback PageKeyed object
     * @param response response from api
     * @param adjacentPageKey Key for subsequent page load
     */
    fun addAfterResultsAndRequestMore(
        callback: PageKeyedDataSource.LoadCallback<Int, Item>?,
        response: List<Item>,
        adjacentPageKey: Int
    )

    /**
     * Shows / Hides the item
     *
     * @param item to be applied the visibility
     * @param visibility true / false to show / hide
     */
    fun setItemVisibility(item: MenuItem?, visibility: Boolean)

    /**
     * Puts list on recyclerview
     *
     * @param listRepositories list of Repository to put on recyclerview
     */
    fun submitList(listRepositories: PagedList<Item>?)

    /**
     * Remove the live data observers
     *
     */
    fun removeLiveDataObservers()

    /**
     * Add observers to the live data
     *
     */
    fun observeLiveData()

    /**
     * Invalidate the repository data source
     *
     */
    fun invalidateDataSource()

    /**
     * Sets menu item drawable given menu, the index to be changed and the drawable
     *
     * @param menu menu to be changed
     * @param index to be changed
     * @param drawable to be changed to
     */
    fun setMenuItem(menu: Menu, index: Int, drawable: Int)
}