package com.nunop.pinkroomgithub.repositoriesfragment.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nunop.pinkroomgithub.R
import com.nunop.pinkroomgithub.databinding.ItemLoadingBinding
import com.nunop.pinkroomgithub.databinding.RepositoryItemSectionBinding
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.repositoriesfragment.DiffUtilCallBack
import com.nunop.pinkroomgithub.utilities.LiveDataState
import com.nunop.pinkroomgithub.utilities.toVisibilityGone
import com.squareup.picasso.Picasso

class RepositoriesAdapter(
    private val onRepositoryListener: OnRepositoryListener
) : PagedListAdapter<Item, RecyclerView.ViewHolder>(DiffUtilCallBack()) {

    private val dataViewType = 1
    private val footerViewType = 2
    private var status = LiveDataState.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == dataViewType) {
            val itemBinding =
                RepositoryItemSectionBinding.inflate(
                    LayoutInflater.from(parent.context), parent,
                    false
                )
            return RepositoryViewHolder(
                itemBinding,
                onRepositoryListener
            )
        } else {
            val itemBinding =
                ItemLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            LoadingViewHolder(itemBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == dataViewType)
            (holder as RepositoryViewHolder).bind(getItem(position))
        else (holder as LoadingViewHolder).bind(status)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) dataViewType else footerViewType
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (status == LiveDataState.LOADING || status == LiveDataState.ERROR)
    }

    fun setState(state: LiveDataState) {
        this.status = state
        notifyItemChanged(super.getItemCount())
    }

    class RepositoryViewHolder(
        private val itemBinding: RepositoryItemSectionBinding,
        private val onRepositoryListener: OnRepositoryListener
    ) : RecyclerView.ViewHolder(itemBinding.root), View.OnClickListener {

        fun bind(item: Item?) {
            if (item != null) {
                if (!item.name.isNullOrBlank()) {
                    itemBinding.tvRepoName.visibility = true.toVisibilityGone()
                    itemBinding.tvRepoName.text = item.name.toString()
                }
                if (!item.description.isNullOrBlank()) {
                    itemBinding.tvRepoDescription.visibility = true.toVisibilityGone()
                    itemBinding.tvRepoDescription.text = item.description.toString()
                }
                if (!item.language.isNullOrBlank()) {
                    itemBinding.tvLanguage.visibility = true.toVisibilityGone()
                    itemBinding.tvLanguage.text = item.language.toString()
                }
                if (item.stargazers_count != null) {
                    itemBinding.ivStars.visibility = true.toVisibilityGone()
                    itemBinding.tvNumberStars.visibility = true.toVisibilityGone()
                    itemBinding.tvNumberStars.text = item.stargazers_count.toString()
                }
                Picasso.get().load(item.owner?.avatar_url).fit()
                    .error(R.drawable.ic_baseline_person_24)
                    .into(itemBinding.ivOwner)
            }
        }

        init {
            itemBinding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onRepositoryListener.onRepositoryClick(adapterPosition)
        }
    }

    class LoadingViewHolder(
        private val itemBinding: ItemLoadingBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(status: LiveDataState?) {
            itemBinding.pbLoadingItem.visibility =
                if (status == LiveDataState.LOADING) View.VISIBLE else View.INVISIBLE
        }
    }

    interface OnRepositoryListener {
        fun onRepositoryClick(position: Int)
    }

}