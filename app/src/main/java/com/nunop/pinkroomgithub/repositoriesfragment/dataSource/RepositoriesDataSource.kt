package com.nunop.pinkroomgithub.repositoriesfragment.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.repositoriesfragment.RepositoriesFragmentPresenter
import com.nunop.pinkroomgithub.utilities.LiveDataState
import java.util.*

class RepositoriesDataSource(
    private val mPresenter: RepositoriesFragmentPresenter
) : PageKeyedDataSource<Int, Item>() {

    var state: MutableLiveData<LiveDataState> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Item>
    ) {
        state.postValue(LiveDataState.LOADING)
        mPresenter.addDisposable(
            mPresenter.loadInitialRequest(params.requestedLoadSize)
                .subscribe({ response ->
                    val code = response.code()
                    val body = response.body()
                    if (code == 200 && body != null && body.items != null && body.items!!.isNotEmpty
                            ()
                    ) {
                        state.postValue(LiveDataState.DONE)
                        mPresenter.loadInitialCode200(
                            body.items!!,
                            params.requestedLoadSize,
                            callback
                        )
                    } else if (code == 204 || code == 200 && (body?.items == null || body.items!!.isEmpty())
                    ) {
                        state.postValue(LiveDataState.NODATA)
                    } else {
                        state.postValue(LiveDataState.ERROR)
                    }
                }, {
                    state.postValue(LiveDataState.ERROR)
                    callback.onResult(ArrayList(), 1, 2)
                })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        if (mPresenter.afterRequestShouldBePerformed()) {
            state.postValue(LiveDataState.LOADING)
            mPresenter.addDisposable(
                mPresenter.loadAfterRequest(params.key, params.requestedLoadSize)
                    .subscribe({ response ->
                        val body = response.body()
                        if (body != null) {
                            state.postValue(LiveDataState.DONE)
                            mPresenter.loadAfterRequestSubscribe(
                                body.items, params.key + 1, params.requestedLoadSize, callback
                            )
                        }
                    }, {
                        state.postValue(LiveDataState.ERROR)
                    })
            )
        } else {
            state.postValue(LiveDataState.DONE)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {}
}