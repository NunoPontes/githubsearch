package com.nunop.pinkroomgithub.repositoriesfragment.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.repositoriesfragment.RepositoriesFragmentPresenter

class RepositoriesDataSourceFactory(
    private val mPresenter: RepositoriesFragmentPresenter
) : DataSource.Factory<Int, Item>() {

    val repositoriesDataSourceLiveData = MutableLiveData<RepositoriesDataSource>()

    override fun create(): DataSource<Int, Item> {
        val repositoriesDataSource = RepositoriesDataSource(mPresenter)
        repositoriesDataSourceLiveData.postValue(repositoriesDataSource)
        return repositoriesDataSource
    }
}
