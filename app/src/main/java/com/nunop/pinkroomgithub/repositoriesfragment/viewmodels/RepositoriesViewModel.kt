package com.nunop.pinkroomgithub.repositoriesfragment.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.repositoriesfragment.RepositoriesFragmentPresenter
import com.nunop.pinkroomgithub.repositoriesfragment.dataSource.RepositoriesDataSource
import com.nunop.pinkroomgithub.repositoriesfragment.dataSource.RepositoriesDataSourceFactory
import com.nunop.pinkroomgithub.utilities.LiveDataState

class RepositoriesViewModel(
    mPresenter: RepositoriesFragmentPresenter
) : ViewModel() {
    var repositoriesLiveData: LiveData<PagedList<Item>>? = null
    private var mRepositoriesDataSourceFactory: RepositoriesDataSourceFactory =
        RepositoriesDataSourceFactory(mPresenter)

    init {
        val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(20)
            .setInitialLoadSizeHint(20)
            .build()
        repositoriesLiveData = LivePagedListBuilder(mRepositoriesDataSourceFactory, config).build()
    }

    fun getState(): LiveData<LiveDataState> = Transformations.switchMap(
        mRepositoriesDataSourceFactory.repositoriesDataSourceLiveData,
        RepositoriesDataSource::state
    )

    fun listIsEmpty(): Boolean {
        return repositoriesLiveData?.value?.isEmpty() ?: true
    }

    fun listIsInvalid(): Boolean {
        return repositoriesLiveData?.value?.dataSource?.isInvalid ?: false
    }
}