package com.nunop.pinkroomgithub.repositoriesfragment.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nunop.pinkroomgithub.repositoriesfragment.RepositoriesFragmentPresenter
import java.util.*

class RepositoriesViewModelFactory(private val mPresenter: RepositoriesFragmentPresenter) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return Objects.requireNonNull(
            modelClass.cast(
                RepositoriesViewModel(
                    mPresenter
                )
            )
        )
    }
}