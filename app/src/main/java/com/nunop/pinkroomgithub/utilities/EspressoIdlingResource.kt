package com.nunop.pinkroomgithub.utilities

import android.util.Log
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.idling.CountingIdlingResource

class EspressoIdlingResource {

    companion object {
        private const val RESOURCE = "GLOBAL"

        private val mCountingIdlingResource = CountingIdlingResource(RESOURCE)

        fun increment() {
            mCountingIdlingResource.increment()
        }

        fun decrement() {
            try {
                if (!isIdle()) {
                    mCountingIdlingResource.decrement()
                }
            } catch (e: Exception) {
                Log.e(this.javaClass.simpleName, e.toString())
            }

        }

        private fun isIdle(): Boolean {
            return mCountingIdlingResource.isIdleNow
        }

        fun getIdlingResource(): IdlingResource {
            return mCountingIdlingResource
        }
    }
}