package com.nunop.pinkroomgithub.utilities

import android.view.View

/**
 * Converts the true/false of a boolean value in View.Visible or View.Gone
 */
fun Boolean.toVisibilityGone() = if (this) View.VISIBLE else View.GONE

