package com.nunop.pinkroomgithub.utilities

enum class LiveDataState {
    DONE, LOADING, ERROR, NODATA
}