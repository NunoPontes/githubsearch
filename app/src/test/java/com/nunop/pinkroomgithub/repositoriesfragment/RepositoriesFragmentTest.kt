package com.nunop.pinkroomgithub.repositoriesfragment

import com.nunop.pinkroomgithub.api.github.GithubModel
import com.nunop.pinkroomgithub.models.repository.Repository
import com.nunop.pinkroomgithub.utilities.LiveDataState
import com.nunop.pinkroomgithub.utils.RxJavaTestRunner
import com.nunop.pinkroomgithub.utils.TestData
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import retrofit2.Response

@RunWith(RxJavaTestRunner::class)
class RepositoriesFragmentTest {

    private lateinit var mView: RepositoriesFragmentView
    private lateinit var mPresenter: RepositoriesFragmentPresenter
    private lateinit var mGithubModel: GithubModel
    private val pageSize = 5
    private val pageNumber = 2

    @Before
    @Throws(Exception::class)
    fun setUp() {
        mView = mock(RepositoriesFragmentView::class.java)
        mGithubModel = mock(GithubModel::class.java)

        mPresenter =
            RepositoriesFragmentPresenterImpl(
                mView,
                mGithubModel
            )
    }

    @Test
    fun resetRepositoryList() {
        mPresenter.resetRepositoryList()

        verify(mView).removeLiveDataObservers()
        verify(mView).invalidateDataSource()
        verify(mView).observeLiveData()

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun loadAfterRequest() {
        val repository = TestData.buildRepository()
        val repositoryResponse: Response<Repository> =
            Response.success(repository)

        doReturn(Observable.just(repositoryResponse)).`when`(mGithubModel)
            .searchRepositories(
                query = "Android",
                sort = "stars",
                order = "desc", pageNumber, pageSize
            )


        mPresenter.loadAfterRequest(pageNumber, pageSize)

        verify(mGithubModel).searchRepositories(
            query = "Android",
            sort = "stars",
            order = "desc",
            pageNumber,
            pageSize
        )

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun loadInitialCode200() {
        val repository = TestData.buildRepository()

        mPresenter.loadInitialCode200(repository.items!!, pageSize, null)

        verify(mView).addInitResultsAndRequestMore(null, repository.items!!, null, 2)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun loadInitialRequest() {
        val repository = TestData.buildRepository()
        val repositoryResponse: Response<Repository> =
            Response.success(repository)

        doReturn(Observable.just(repositoryResponse)).`when`(mGithubModel)
            .searchRepositories(
                query = "Android",
                sort = "stars",
                order = "desc", pageNumber, pageSize
            )

        mPresenter.loadInitialRequest(pageSize)

        verify(mGithubModel).searchRepositories(
            query = "Android",
            sort = "stars",
            order = "desc",
            1,
            pageSize
        )

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun stateObserverLoadingListEmptyTrueListIsInvalidateTrue() {
        `when`(mView.listIsInvalidated()).thenReturn(true)
        `when`(mView.listIsEmpty()).thenReturn(true)

        mPresenter.stateObserver(LiveDataState.LOADING)

        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(false)
        verify(mView).setProgressBarVisibility(true)
        verify(mView, times(2)).listIsEmpty()

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun stateObserverLoadingListEmptyFalseListIsInvalidateFalse() {
        `when`(mView.listIsInvalidated()).thenReturn(false)
        `when`(mView.listIsEmpty()).thenReturn(false)

        mPresenter.stateObserver(LiveDataState.LOADING)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).setAdapterState(LiveDataState.LOADING)
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun stateObserverErrorListIsEmptyTrueListIsInvalidateTrue() {
        `when`(mView.listIsInvalidated()).thenReturn(true)
        `when`(mView.listIsEmpty()).thenReturn(true)

        mPresenter.stateObserver(LiveDataState.ERROR)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun stateObserverErrorListIsEmptyFalseListIsInvalidateFalse() {
        `when`(mView.listIsInvalidated()).thenReturn(false)
        `when`(mView.listIsEmpty()).thenReturn(false)

        mPresenter.stateObserver(LiveDataState.ERROR)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setAdapterState(LiveDataState.ERROR)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun stateObserverNoDataListIsEmptyTrueListIsInvalidateTrue() {
        `when`(mView.listIsInvalidated()).thenReturn(true)
        `when`(mView.listIsEmpty()).thenReturn(true)

        mPresenter.stateObserver(LiveDataState.NODATA)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)

    }

    @Test
    fun stateObserverNoDataListIsEmptyFalseListIsInvalidateFalse() {
        `when`(mView.listIsInvalidated()).thenReturn(false)
        `when`(mView.listIsEmpty()).thenReturn(false)

        mPresenter.stateObserver(LiveDataState.NODATA)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setAdapterState(LiveDataState.NODATA)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun stateObserverOtherListEmptyTrue() {
        `when`(mView.listIsInvalidated()).thenReturn(true)
        `when`(mView.listIsEmpty()).thenReturn(true)

        mPresenter.stateObserver(LiveDataState.DONE)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    fun stateObserverOtherListEmptyFalse() {

        `when`(mView.listIsInvalidated()).thenReturn(true)
        `when`(mView.listIsEmpty()).thenReturn(false)

        mPresenter.stateObserver(LiveDataState.DONE)

        verify(mView, times(2)).listIsEmpty()
        verify(mView).listIsInvalidated()
        verify(mView).setListVisibility(true)
        verify(mView).setAdapterState(LiveDataState.DONE)
        verify(mView).setProgressBarVisibility(false)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

    @Test
    fun loadAfterRequestSubscribeRepeatedRepository() {
        val repository = TestData.buildRepository()

        mPresenter.loadAfterRequestSubscribe(repository.items, 5, pageSize, null)

        verify(mView).addAfterResultsAndRequestMore(null, repository.items!!, 5)

        verifyNoMoreInteractions(mView)
        verifyNoMoreInteractions(mGithubModel)
    }

}