package com.nunop.pinkroomgithub.utils

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import junitparams.JUnitParamsRunner
import org.mockito.junit.MockitoJUnitRunner

/**
 * RxJava test runner to check observable which runs on the main thread. This runner extends [MockitoJUnitRunner].
 */
class RxJavaTestRunner(clazz: Class<*>) : JUnitParamsRunner(clazz) {

    init {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
    }
}