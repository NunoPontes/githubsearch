package com.nunop.pinkroomgithub.utils

import com.nunop.pinkroomgithub.models.repository.Item
import com.nunop.pinkroomgithub.models.repository.Repository

object TestData {

    fun buildRepository() = Repository().apply {
        total_count = 1
        incomplete_results = false
        items = listOf(
            Item(
                id = 1,
                name = "Tetris",
                description = "Tetris game",
                stargazers_count = 12
            )
        )
    }
}